CREATE DATABASE  IF NOT EXISTS `prepa_mx` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `prepa_mx`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: prepa_mx
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alu_alumno`
--

DROP TABLE IF EXISTS `alu_alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `alu_alumno` (
  `id_alumno` int(11) NOT NULL AUTO_INCREMENT,
  `no_control` varchar(15) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `ape_pat` varchar(20) NOT NULL,
  `ape_mat` varchar(20) DEFAULT NULL,
  `curp` varchar(18) NOT NULL,
  `telefono` int(13) NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `correo` text,
  `id_domicilio` int(11) NOT NULL,
  PRIMARY KEY (`id_alumno`),
  KEY `alumno-domicilio_idx` (`id_domicilio`),
  CONSTRAINT `alumno-domicilio` FOREIGN KEY (`id_domicilio`) REFERENCES `alu_domicilio` (`id_domicilio`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alu_alumno`
--

LOCK TABLES `alu_alumno` WRITE;
/*!40000 ALTER TABLE `alu_alumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `alu_alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alu_carga_academica`
--

DROP TABLE IF EXISTS `alu_carga_academica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `alu_carga_academica` (
  `id_carga_academica` int(11) NOT NULL AUTO_INCREMENT,
  `hora_entrada` time NOT NULL,
  `hora_salida` time NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  `id_nivel` int(11) NOT NULL,
  PRIMARY KEY (`id_carga_academica`),
  KEY `carga_academica-usuario_idx` (`id_usuario`),
  KEY `carga_academica-alumno_idx` (`id_alumno`),
  KEY `carga_academica-nivel_idx` (`id_nivel`),
  CONSTRAINT `carga_academica-alumno` FOREIGN KEY (`id_alumno`) REFERENCES `alu_alumno` (`id_alumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `carga_academica-nivel` FOREIGN KEY (`id_nivel`) REFERENCES `bas_niveles` (`id_nivel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `carga_academica-usuario` FOREIGN KEY (`id_usuario`) REFERENCES `gen_usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alu_carga_academica`
--

LOCK TABLES `alu_carga_academica` WRITE;
/*!40000 ALTER TABLE `alu_carga_academica` DISABLE KEYS */;
/*!40000 ALTER TABLE `alu_carga_academica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alu_det_carga_academica`
--

DROP TABLE IF EXISTS `alu_det_carga_academica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `alu_det_carga_academica` (
  `id_det_carga_academica` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_curso` varchar(15) NOT NULL,
  `fecha_registro_materia` datetime NOT NULL,
  `id_det_materia` int(11) NOT NULL,
  `id_carga_academica` int(11) NOT NULL,
  PRIMARY KEY (`id_det_carga_academica`),
  KEY `det_carga_academica-det_materia_idx` (`id_det_materia`),
  KEY `det_carga_academica-carga_academica_idx` (`id_carga_academica`),
  CONSTRAINT `det_carga_academica-carga_academica` FOREIGN KEY (`id_carga_academica`) REFERENCES `alu_carga_academica` (`id_carga_academica`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `det_carga_academica-det_materia` FOREIGN KEY (`id_det_materia`) REFERENCES `mat_det_materia` (`id_det_materia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alu_det_carga_academica`
--

LOCK TABLES `alu_det_carga_academica` WRITE;
/*!40000 ALTER TABLE `alu_det_carga_academica` DISABLE KEYS */;
/*!40000 ALTER TABLE `alu_det_carga_academica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alu_documentacion`
--

DROP TABLE IF EXISTS `alu_documentacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `alu_documentacion` (
  `id_documentacion` int(11) NOT NULL AUTO_INCREMENT,
  `curp` bit(1) DEFAULT NULL,
  `acta_nacimiento` bit(1) DEFAULT NULL,
  `certificado` bit(1) DEFAULT NULL,
  `id_alumno` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_documentacion`),
  KEY `documentacion-alumno_idx` (`id_alumno`),
  CONSTRAINT `documentacion-alumno` FOREIGN KEY (`id_alumno`) REFERENCES `alu_alumno` (`id_alumno`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alu_documentacion`
--

LOCK TABLES `alu_documentacion` WRITE;
/*!40000 ALTER TABLE `alu_documentacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `alu_documentacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alu_domicilio`
--

DROP TABLE IF EXISTS `alu_domicilio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `alu_domicilio` (
  `id_domicilio` int(11) NOT NULL AUTO_INCREMENT,
  `calle` varchar(45) NOT NULL,
  `num_int` varchar(5) DEFAULT NULL,
  `num_ext` varchar(5) NOT NULL,
  `cp` int(6) NOT NULL,
  `entre_calle` varchar(45) NOT NULL,
  `referencias` text NOT NULL,
  `id_estado` varchar(2) NOT NULL,
  `id_municipio` int(10) NOT NULL,
  PRIMARY KEY (`id_domicilio`),
  KEY `domicilio-municipio_idx` (`id_municipio`),
  KEY `domicilio-estado_idx` (`id_estado`),
  CONSTRAINT `domicilio-municipio` FOREIGN KEY (`id_municipio`) REFERENCES `gen_municipios` (`ID_MUN`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alu_domicilio`
--

LOCK TABLES `alu_domicilio` WRITE;
/*!40000 ALTER TABLE `alu_domicilio` DISABLE KEYS */;
/*!40000 ALTER TABLE `alu_domicilio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alu_tutor`
--

DROP TABLE IF EXISTS `alu_tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `alu_tutor` (
  `id_tutor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `ape_pat` varchar(20) NOT NULL,
  `ape_mat` varchar(20) DEFAULT NULL,
  `telefono` int(13) NOT NULL,
  `correo` text,
  `id_domicilio_particular` int(11) NOT NULL,
  `id_domicilio_laboral` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  PRIMARY KEY (`id_tutor`),
  KEY `tutor-domicilio_particular_idx` (`id_domicilio_particular`),
  KEY `tutor-domicilio_laboral_idx` (`id_domicilio_laboral`),
  KEY `tutor-alumno_idx` (`id_alumno`),
  CONSTRAINT `tutor-alumno` FOREIGN KEY (`id_alumno`) REFERENCES `alu_alumno` (`id_alumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tutor-domicilio_laboral` FOREIGN KEY (`id_domicilio_laboral`) REFERENCES `alu_domicilio` (`id_domicilio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tutor-domicilio_particular` FOREIGN KEY (`id_domicilio_particular`) REFERENCES `alu_domicilio` (`id_domicilio`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alu_tutor`
--

LOCK TABLES `alu_tutor` WRITE;
/*!40000 ALTER TABLE `alu_tutor` DISABLE KEYS */;
/*!40000 ALTER TABLE `alu_tutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bas_ciclo`
--

DROP TABLE IF EXISTS `bas_ciclo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bas_ciclo` (
  `id_ciclo` int(11) NOT NULL AUTO_INCREMENT,
  `ciclo` varchar(30) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_ciclo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bas_ciclo`
--

LOCK TABLES `bas_ciclo` WRITE;
/*!40000 ALTER TABLE `bas_ciclo` DISABLE KEYS */;
/*!40000 ALTER TABLE `bas_ciclo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bas_niveles`
--

DROP TABLE IF EXISTS `bas_niveles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bas_niveles` (
  `id_nivel` int(11) NOT NULL AUTO_INCREMENT,
  `grado` varchar(15) NOT NULL,
  `grupo` varchar(15) NOT NULL,
  `status` bit(1) NOT NULL,
  `id_periodo` int(11) NOT NULL,
  PRIMARY KEY (`id_nivel`),
  KEY `niveles-periodo_idx` (`id_periodo`),
  CONSTRAINT `niveles-periodo` FOREIGN KEY (`id_periodo`) REFERENCES `bas_periodo` (`id_periodo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bas_niveles`
--

LOCK TABLES `bas_niveles` WRITE;
/*!40000 ALTER TABLE `bas_niveles` DISABLE KEYS */;
/*!40000 ALTER TABLE `bas_niveles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bas_periodo`
--

DROP TABLE IF EXISTS `bas_periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bas_periodo` (
  `id_periodo` int(11) NOT NULL AUTO_INCREMENT,
  `periodo` varchar(30) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `id_ciclo` int(11) NOT NULL,
  PRIMARY KEY (`id_periodo`),
  KEY `periodo-ciclo_idx` (`id_ciclo`),
  CONSTRAINT `periodo-ciclo` FOREIGN KEY (`id_ciclo`) REFERENCES `bas_ciclo` (`id_ciclo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bas_periodo`
--

LOCK TABLES `bas_periodo` WRITE;
/*!40000 ALTER TABLE `bas_periodo` DISABLE KEYS */;
/*!40000 ALTER TABLE `bas_periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cal_calificacion`
--

DROP TABLE IF EXISTS `cal_calificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cal_calificacion` (
  `id_calificacion` int(11) NOT NULL AUTO_INCREMENT,
  `inicio_evaluacion` date NOT NULL,
  `fin_evaluacion` date NOT NULL,
  `fecha_recibio` date NOT NULL,
  `id_alumno` int(11) NOT NULL,
  PRIMARY KEY (`id_calificacion`),
  KEY `calificacion-alumno_idx` (`id_alumno`),
  CONSTRAINT `calificacion-alumno` FOREIGN KEY (`id_alumno`) REFERENCES `alu_alumno` (`id_alumno`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cal_calificacion`
--

LOCK TABLES `cal_calificacion` WRITE;
/*!40000 ALTER TABLE `cal_calificacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `cal_calificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cal_det_calificacion`
--

DROP TABLE IF EXISTS `cal_det_calificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cal_det_calificacion` (
  `id_det_calificacion` int(11) NOT NULL AUTO_INCREMENT,
  `calificacion` float NOT NULL,
  `observaciones` text NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_calificacion` int(11) NOT NULL,
  `id_det_materia` int(11) NOT NULL,
  PRIMARY KEY (`id_det_calificacion`),
  KEY `det_calificacion-usuario_idx` (`id_usuario`),
  KEY `det_calificacion-calificacion_idx` (`id_calificacion`),
  KEY `det_calificacion-det_materia_idx` (`id_det_materia`),
  CONSTRAINT `det_calificacion-calificacion` FOREIGN KEY (`id_calificacion`) REFERENCES `cal_calificacion` (`id_calificacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `det_calificacion-det_materia` FOREIGN KEY (`id_det_materia`) REFERENCES `mat_det_materia` (`id_det_materia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `det_calificacion-usuario` FOREIGN KEY (`id_usuario`) REFERENCES `gen_usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cal_det_calificacion`
--

LOCK TABLES `cal_det_calificacion` WRITE;
/*!40000 ALTER TABLE `cal_det_calificacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `cal_det_calificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `con_adeudo`
--

DROP TABLE IF EXISTS `con_adeudo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `con_adeudo` (
  `id_adeudo` int(11) NOT NULL AUTO_INCREMENT,
  `monto_total` double NOT NULL,
  `pago_minimo` double NOT NULL,
  `status` bit(1) NOT NULL,
  `id_concepto` int(11) NOT NULL,
  `id_alumno` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_adeudo`),
  KEY `adeudo-concepto_idx` (`id_concepto`),
  KEY `adeudo-alumno_idx` (`id_alumno`),
  CONSTRAINT `adeudo-alumno` FOREIGN KEY (`id_alumno`) REFERENCES `alu_alumno` (`id_alumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `adeudo-concepto` FOREIGN KEY (`id_concepto`) REFERENCES `con_conceptos` (`id_concepto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `con_adeudo`
--

LOCK TABLES `con_adeudo` WRITE;
/*!40000 ALTER TABLE `con_adeudo` DISABLE KEYS */;
/*!40000 ALTER TABLE `con_adeudo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `con_conceptos`
--

DROP TABLE IF EXISTS `con_conceptos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `con_conceptos` (
  `id_concepto` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `costo` float NOT NULL,
  `recargo` float NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_concepto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `con_conceptos`
--

LOCK TABLES `con_conceptos` WRITE;
/*!40000 ALTER TABLE `con_conceptos` DISABLE KEYS */;
/*!40000 ALTER TABLE `con_conceptos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `con_det_adeudo`
--

DROP TABLE IF EXISTS `con_det_adeudo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `con_det_adeudo` (
  `id_det_adeudo` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_recepcion` datetime NOT NULL,
  `saldo_anterior` double NOT NULL,
  `monto_pagado` double NOT NULL,
  `fecha_nuevo_pago` date DEFAULT NULL,
  `id_adeudo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_det_adeudo`),
  KEY `det_adeudo-adeudo_idx` (`id_adeudo`),
  KEY `det_adeudo-usuario_idx` (`id_usuario`),
  CONSTRAINT `det_adeudo-adeudo` FOREIGN KEY (`id_adeudo`) REFERENCES `con_adeudo` (`id_adeudo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `det_adeudo-usuario` FOREIGN KEY (`id_usuario`) REFERENCES `gen_usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `con_det_adeudo`
--

LOCK TABLES `con_det_adeudo` WRITE;
/*!40000 ALTER TABLE `con_det_adeudo` DISABLE KEYS */;
/*!40000 ALTER TABLE `con_det_adeudo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `con_gastos`
--

DROP TABLE IF EXISTS `con_gastos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `con_gastos` (
  `id_gasto` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_gasto` varchar(45) NOT NULL,
  `descripcion` text NOT NULL,
  `monto_pagar` float NOT NULL,
  `fecha_limite` date NOT NULL,
  `fecha_pago` date DEFAULT NULL,
  `fecha_origino` date DEFAULT NULL,
  `liquidado` bit(1) NOT NULL DEFAULT b'0',
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_gasto`),
  KEY `gastos-usuario_idx` (`id_usuario`),
  CONSTRAINT `gastos-usuario` FOREIGN KEY (`id_usuario`) REFERENCES `gen_usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `con_gastos`
--

LOCK TABLES `con_gastos` WRITE;
/*!40000 ALTER TABLE `con_gastos` DISABLE KEYS */;
/*!40000 ALTER TABLE `con_gastos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `con_inscripcion`
--

DROP TABLE IF EXISTS `con_inscripcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `con_inscripcion` (
  `id_inscripcion` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_inscripcion` datetime NOT NULL,
  `turno` varchar(15) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  `id_periodo` int(11) NOT NULL,
  `id_carga_academica` int(11) NOT NULL,
  PRIMARY KEY (`id_inscripcion`),
  KEY `inscripcion-alumno_idx` (`id_alumno`),
  KEY `inscripcion-periodo_idx` (`id_periodo`),
  KEY `inscripcion-carga_academica_idx` (`id_carga_academica`),
  CONSTRAINT `inscripcion-alumno` FOREIGN KEY (`id_alumno`) REFERENCES `gen_usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `inscripcion-carga_academica` FOREIGN KEY (`id_carga_academica`) REFERENCES `alu_carga_academica` (`id_carga_academica`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `inscripcion-periodo` FOREIGN KEY (`id_periodo`) REFERENCES `bas_periodo` (`id_periodo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `con_inscripcion`
--

LOCK TABLES `con_inscripcion` WRITE;
/*!40000 ALTER TABLE `con_inscripcion` DISABLE KEYS */;
/*!40000 ALTER TABLE `con_inscripcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gen_estados`
--

DROP TABLE IF EXISTS `gen_estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `gen_estados` (
  `CVE_ENT` varchar(2) NOT NULL,
  `NOM_ENT` varchar(110) NOT NULL,
  `NOM_ABR` varchar(32) NOT NULL,
  PRIMARY KEY (`CVE_ENT`),
  KEY `CVE_ENT` (`CVE_ENT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_estados`
--

LOCK TABLES `gen_estados` WRITE;
/*!40000 ALTER TABLE `gen_estados` DISABLE KEYS */;
INSERT INTO `gen_estados` VALUES ('01','AGUASCALIENTES','AGS.'),('02','BAJA CALIFORNIA','BC'),('03','BAJA CALIFORNIA SUR','BCS'),('04','CAMPECHE','CAMP.'),('05','COAHUILA DE ZARAGOZA','COAH.'),('06','COLIMA','COL.'),('07','CHIAPAS','CHIS.'),('08','CHIHUAHUA','CHIH.'),('09','DISTRITO FEDERAL','DF'),('10','DURANGO','DGO.'),('11','GUANAJUATO','GTO.'),('12','GUERRERO','GRO.'),('13','HIDALGO','HGO.'),('14','JALISCO','JAL.'),('15','MEXICO','MEX.'),('16','MICHOACAN DE OCAMPO','MICH.'),('17','MORELOS','MOR.'),('18','NAYARIT','NAY.'),('19','NUEVO LEON','NL'),('20','OAXACA','OAX.'),('21','PUEBLA','PUE.'),('22','QUERETARO','QRO.'),('23','QUINTANA ROO','Q. ROO'),('24','SAN LUIS POTOSI','SLP'),('25','SINALOA','SIN.'),('26','SONORA','SON.'),('27','TABASCO','TAB.'),('28','TAMAULIPAS','TAMPS.'),('29','TLAXCALA','TLAX.'),('30','VERACRUZ DE IGNACIO DE LA LLAVE','VER.'),('31','YUCATAN','YUC.'),('32','ZACATECAS','ZAC.');
/*!40000 ALTER TABLE `gen_estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gen_municipios`
--

DROP TABLE IF EXISTS `gen_municipios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `gen_municipios` (
  `CVE_ENT` varchar(2) NOT NULL,
  `CVE_MUN` varchar(3) NOT NULL,
  `NOM_MUN` varchar(110) NOT NULL,
  `ID_MUN` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID_MUN`),
  KEY `CVE_ENT` (`CVE_ENT`),
  KEY `CVE_MUN` (`CVE_MUN`)
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_municipios`
--

LOCK TABLES `gen_municipios` WRITE;
/*!40000 ALTER TABLE `gen_municipios` DISABLE KEYS */;
INSERT INTO `gen_municipios` VALUES ('01','001','AGUASCALIENTES',2),('02','001','ENSENADA',3),('03','001','COMONDU',4),('04','001','CALKINI',5),('05','001','ABASOLO',6),('06','001','ARMERIA',7),('07','001','ACACOYAGUA',8),('08','001','AHUMADA',9),('10','001','CANATLAN',10),('11','001','ABASOLO',11),('12','001','ACAPULCO DE JUAREZ',12),('13','001','ACATLAN',13),('14','001','ACATIC',14),('15','001','ACAMBAY',15),('16','001','ACUITZIO',16),('17','001','AMACUZAC',17),('18','001','ACAPONETA',18),('19','001','ABASOLO',19),('20','001','ABEJONES',20),('21','001','ACAJETE',21),('22','001','AMEALCO DE BONFIL',22),('23','001','COZUMEL',23),('24','001','AHUALULCO',24),('25','001','AHOME',25),('26','001','ACONCHI',26),('27','001','BALANCAN',27),('28','001','ABASOLO',28),('29','001','AMAXAC DE GUERRERO',29),('30','001','ACAJETE',30),('31','001','ABALA',31),('32','001','APOZOL',32),('01','002','ASIENTOS',33),('02','002','MEXICALI',34),('03','002','MULEGE',35),('04','002','CAMPECHE',36),('05','002','ACU¥A',37),('06','002','COLIMA',38),('07','002','ACALA',39),('08','002','ALDAMA',40),('09','002','AZCAPOTZALCO',41),('10','002','CANELAS',42),('11','002','ACAMBARO',43),('12','002','AHUACUOTZINGO',44),('13','002','ACAXOCHITLAN',45),('14','002','ACATLAN DE JUAREZ',46),('15','002','ACOLMAN',47),('16','002','AGUILILLA',48),('17','002','ATLATLAHUCAN',49),('18','002','AHUACATLAN',50),('19','002','AGUALEGUAS',51),('20','002','ACATLAN DE PEREZ FIGUEROA',52),('21','002','ACATENO',53),('22','002','PINAL DE AMOLES',54),('23','002','FELIPE CARRILLO PUERTO',55),('24','002','ALAQUINES',56),('25','002','ANGOSTURA',57),('26','002','AGUA PRIETA',58),('27','002','CARDENAS',59),('28','002','ALDAMA',60),('29','002','APETATITLAN DE ANTONIO CARVAJAL',61),('30','002','ACATLAN',62),('31','002','ACANCEH',63),('32','002','APULCO',64),('01','003','CALVILLO',65),('02','003','TECATE',66),('03','003','LA PAZ',67),('04','003','CARMEN',68),('05','003','ALLENDE',69),('06','003','COMALA',70),('07','003','ACAPETAHUA',71),('08','003','ALLENDE',72),('09','003','COYOACAN',73),('10','003','CONETO DE COMONFORT',74),('11','003','SAN MIGUEL DE ALLENDE',75),('12','003','AJUCHITLAN DEL PROGRESO',76),('13','003','ACTOPAN',77),('14','003','AHUALULCO DE MERCADO',78),('15','003','ACULCO',79),('16','003','ALVARO OBREGON',80),('17','003','AXOCHIAPAN',81),('18','003','AMATLAN DE CA¥AS',82),('19','003','LOS ALDAMAS',83),('20','003','ASUNCION CACALOTEPEC',84),('21','003','ACATLAN',85),('22','003','ARROYO SECO',86),('23','003','ISLA MUJERES',87),('24','003','AQUISMON',88),('25','003','BADIRAGUATO',89),('26','003','ALAMOS',90),('27','003','CENTLA',91),('28','003','ALTAMIRA',92),('29','003','ATLANGATEPEC',93),('30','003','ACAYUCAN',94),('31','003','AKIL',95),('32','003','ATOLINGA',96),('01','004','COSIO',97),('02','004','TIJUANA',98),('04','004','CHAMPOTON',99),('05','004','ARTEAGA',100),('06','004','COQUIMATLAN',101),('07','004','ALTAMIRANO',102),('08','004','AQUILES SERDAN',103),('09','004','CUAJIMALPA DE MORELOS',104),('10','004','CUENCAME',105),('11','004','APASEO EL ALTO',106),('12','004','ALCOZAUCA DE GUERRERO',107),('13','004','AGUA BLANCA DE ITURBIDE',108),('14','004','AMACUECA',109),('15','004','ALMOLOYA DE ALQUISIRAS',110),('16','004','ANGAMACUTIRO',111),('17','004','AYALA',112),('18','004','COMPOSTELA',113),('19','004','ALLENDE',114),('20','004','ASUNCION CUYOTEPEJI',115),('21','004','ACATZINGO',116),('22','004','CADEREYTA DE MONTES',117),('23','004','OTHON P. BLANCO',118),('24','004','ARMADILLO DE LOS INFANTE',119),('25','004','CONCORDIA',120),('26','004','ALTAR',121),('27','004','CENTRO',122),('28','004','ANTIGUO MORELOS',123),('29','004','ATLTZAYANCA',124),('30','004','ACTOPAN',125),('31','004','BACA',126),('32','004','BENITO JUAREZ',127),('01','005','JESUS MARIA',128),('02','005','PLAYAS DE ROSARITO',129),('04','005','HECELCHAKAN',130),('05','005','CANDELA',131),('06','005','CUAUHTEMOC',132),('07','005','AMATAN',133),('08','005','ASCENSION',134),('09','005','GUSTAVO A. MADERO',135),('10','005','DURANGO',136),('11','005','APASEO EL GRANDE',137),('12','005','ALPOYECA',138),('13','005','AJACUBA',139),('14','005','AMATITAN',140),('15','005','ALMOLOYA DE JUAREZ',141),('16','005','ANGANGUEO',142),('17','005','COATLAN DEL RIO',143),('18','005','HUAJICORI',144),('19','005','ANAHUAC',145),('20','005','ASUNCION IXTALTEPEC',146),('21','005','ACTEOPAN',147),('22','005','COLON',148),('23','005','BENITO JUAREZ',149),('24','005','CARDENAS',150),('25','005','COSALA',151),('26','005','ARIVECHI',152),('27','005','COMALCALCO',153),('28','005','BURGOS',154),('29','005','APIZACO',155),('30','005','ACULA',156),('31','005','BOKOBA',157),('32','005','CALERA',158),('01','006','PABELLON DE ARTEAGA',159),('04','006','HOPELCHEN',160),('05','006','CASTA¥OS',161),('06','006','IXTLAHUACAN',162),('07','006','AMATENANGO DE LA FRONTERA',163),('08','006','BACHINIVA',164),('09','006','IZTACALCO',165),('10','006','GENERAL SIMON BOLIVAR',166),('11','006','ATARJEA',167),('12','006','APAXTLA',168),('13','006','ALFAJAYUCAN',169),('14','006','AMECA',170),('15','006','ALMOLOYA DEL RIO',171),('16','006','APATZINGAN',172),('17','006','CUAUTLA',173),('18','006','IXTLAN DEL RIO',174),('19','006','APODACA',175),('20','006','ASUNCION NOCHIXTLAN',176),('21','006','AHUACATLAN',177),('22','006','CORREGIDORA',178),('23','006','JOSE MARIA MORELOS',179),('24','006','CATORCE',180),('25','006','CULIACAN',181),('26','006','ARIZPE',182),('27','006','CUNDUACAN',183),('28','006','BUSTAMANTE',184),('29','006','CALPULALPAN',185),('30','006','ACULTZINGO',186),('31','006','BUCTZOTZ',187),('32','006','CA¥ITAS DE FELIPE PESCADOR',188),('01','007','RINCON DE ROMOS',189),('04','007','PALIZADA',190),('05','007','CUATRO CIENEGAS',191),('06','007','MANZANILLO',192),('07','007','AMATENANGO DEL VALLE',193),('08','007','BALLEZA',194),('09','007','IZTAPALAPA',195),('10','007','GOMEZ PALACIO',196),('11','007','CELAYA',197),('12','007','ARCELIA',198),('13','007','ALMOLOYA',199),('14','007','SAN JUANITO DE ESCOBEDO',200),('15','007','AMANALCO',201),('16','007','APORO',202),('17','007','CUERNAVACA',203),('18','007','JALA',204),('19','007','ARAMBERRI',205),('20','007','ASUNCION OCOTLAN',206),('21','007','AHUATLAN',207),('22','007','EZEQUIEL MONTES',208),('23','007','LAZARO CARDENAS',209),('24','007','CEDRAL',210),('25','007','CHOIX',211),('26','007','ATIL',212),('27','007','EMILIANO ZAPATA',213),('28','007','CAMARGO',214),('29','007','EL CARMEN TEQUEXQUITLA',215),('30','007','CAMARON DE TEJEDA',216),('31','007','CACALCHEN',217),('32','007','CONCEPCION DEL ORO',218),('01','008','SAN JOSE DE GRACIA',219),('03','008','LOS CABOS',220),('04','008','TENABO',221),('05','008','ESCOBEDO',222),('06','008','MINATITLAN',223),('07','008','ANGEL ALBINO CORZO',224),('08','008','BATOPILAS',225),('09','008','LA MAGDALENA CONTRERAS',226),('10','008','GUADALUPE VICTORIA',227),('11','008','MANUEL DOBLADO',228),('12','008','ATENANGO DEL RIO',229),('13','008','APAN',230),('14','008','ARANDAS',231),('15','008','AMATEPEC',232),('16','008','AQUILA',233),('17','008','EMILIANO ZAPATA',234),('18','008','XALISCO',235),('19','008','BUSTAMANTE',236),('20','008','ASUNCION TLACOLULITA',237),('21','008','AHUAZOTEPEC',238),('22','008','HUIMILPAN',239),('23','008','SOLIDARIDAD',240),('24','008','CERRITOS',241),('25','008','ELOTA',242),('26','008','BACADEHUACHI',243),('27','008','HUIMANGUILLO',244),('28','008','CASAS',245),('29','008','CUAPIAXTLA',246),('30','008','ALPATLAHUAC',247),('31','008','CALOTMUL',248),('32','008','CUAUHTEMOC',249),('01','009','TEPEZALA',250),('03','009','LORETO',251),('04','009','ESCARCEGA',252),('05','009','FRANCISCO I. MADERO',253),('06','009','TECOMAN',254),('07','009','ARRIAGA',255),('08','009','BOCOYNA',256),('09','009','MILPA ALTA',257),('10','009','GUANACEVI',258),('11','009','COMONFORT',259),('12','009','ATLAMAJALCINGO DEL MONTE',260),('13','009','EL ARENAL',261),('14','009','EL ARENAL',262),('15','009','AMECAMECA',263),('16','009','ARIO',264),('17','009','HUITZILAC',265),('18','009','DEL NAYAR',266),('19','009','CADEREYTA JIMENEZ',267),('20','009','AYOTZINTEPEC',268),('21','009','AHUEHUETITLA',269),('22','009','JALPAN DE SERRA',270),('23','009','TULUM',271),('24','009','CERRO DE SAN PEDRO',272),('25','009','ESCUINAPA',273),('26','009','BACANORA',274),('27','009','JALAPA',275),('28','009','CIUDAD MADERO',276),('29','009','CUAXOMULCO',277),('30','009','ALTO LUCERO DE GUTIERREZ BARRIOS',278),('31','009','CANSAHCAB',279),('32','009','CHALCHIHUITES',280),('01','010','EL LLANO',281),('04','010','CALAKMUL',282),('05','010','FRONTERA',283),('06','010','VILLA DE ALVAREZ',284),('07','010','BEJUCAL DE OCAMPO',285),('08','010','BUENAVENTURA',286),('09','010','ALVARO OBREGON',287),('10','010','HIDALGO',288),('11','010','CORONEO',289),('12','010','ATLIXTAC',290),('13','010','ATITALAQUIA',291),('14','010','ATEMAJAC DE BRIZUELA',292),('15','010','APAXCO',293),('16','010','ARTEAGA',294),('17','010','JANTETELCO',295),('18','010','ROSAMORADA',296),('19','010','EL CARMEN',297),('20','010','EL BARRIO DE LA SOLEDAD',298),('21','010','AJALPAN',299),('22','010','LANDA DE MATAMOROS',300),('23','010','BACALAR',301),('24','010','CIUDAD DEL MAIZ',302),('25','010','EL FUERTE',303),('26','010','BACERAC',304),('27','010','JALPA DE MENDEZ',305),('28','010','CRUILLAS',306),('29','010','CHIAUTEMPAN',307),('30','010','ALTOTONGA',308),('31','010','CANTAMAYEC',309),('32','010','FRESNILLO',310),('01','011','SAN FRANCISCO DE LOS ROMO',311),('04','011','CANDELARIA',312),('05','011','GENERAL CEPEDA',313),('07','011','BELLA VISTA',314),('08','011','CAMARGO',315),('09','011','TLAHUAC',316),('10','011','INDE',317),('11','011','CORTAZAR',318),('12','011','ATOYAC DE ALVAREZ',319),('13','011','ATLAPEXCO',320),('14','011','ATENGO',321),('15','011','ATENCO',322),('16','011','BRISE¥AS',323),('17','011','JIUTEPEC',324),('18','011','RUIZ',325),('19','011','CERRALVO',326),('20','011','CALIHUALA',327),('21','011','ALBINO ZERTUCHE',328),('22','011','EL MARQUES',329),('24','011','CIUDAD FERNANDEZ',330),('25','011','GUASAVE',331),('26','011','BACOACHI',332),('27','011','JONUTA',333),('28','011','GOMEZ FARIAS',334),('29','011','MU¥OZ DE DOMINGO ARENAS',335),('30','011','ALVARADO',336),('31','011','CELESTUN',337),('32','011','TRINIDAD GARCIA DE LA CADENA',338),('05','012','GUERRERO',339),('07','012','BERRIOZABAL',340),('08','012','CARICHI',341),('09','012','TLALPAN',342),('10','012','LERDO',343),('11','012','CUERAMARO',344),('12','012','AYUTLA DE LOS LIBRES',345),('13','012','ATOTONILCO EL GRANDE',346),('14','012','ATENGUILLO',347),('15','012','ATIZAPAN',348),('16','012','BUENAVISTA',349),('17','012','JOJUTLA',350),('18','012','SAN BLAS',351),('19','012','CIENEGA DE FLORES',352),('20','012','CANDELARIA LOXICHA',353),('21','012','ALJOJUCA',354),('22','012','PEDRO ESCOBEDO',355),('24','012','TANCANHUITZ',356),('25','012','MAZATLAN',357),('26','012','BACUM',358),('27','012','MACUSPANA',359),('28','012','GONZALEZ',360),('29','012','ESPA¥ITA',361),('30','012','AMATITLAN',362),('31','012','CENOTILLO',363),('32','012','GENARO CODINA',364),('05','013','HIDALGO',365),('07','013','BOCHIL',366),('08','013','CASAS GRANDES',367),('09','013','XOCHIMILCO',368),('10','013','MAPIMI',369),('11','013','DOCTOR MORA',370),('12','013','AZOYU',371),('13','013','ATOTONILCO DE TULA',372),('14','013','ATOTONILCO EL ALTO',373),('15','013','ATIZAPAN DE ZARAGOZA',374),('16','013','CARACUARO',375),('17','013','JONACATEPEC',376),('18','013','SAN PEDRO LAGUNILLAS',377),('19','013','CHINA',378),('20','013','CIENEGA DE ZIMATLAN',379),('21','013','ALTEPEXI',380),('22','013','PE¥AMILLER',381),('24','013','CIUDAD VALLES',382),('25','013','MOCORITO',383),('26','013','BANAMICHI',384),('27','013','NACAJUCA',385),('28','013','GÜEMEZ',386),('29','013','HUAMANTLA',387),('30','013','NARANJOS AMATLAN',388),('31','013','CONKAL',389),('32','013','GENERAL ENRIQUE ESTRADA',390),('05','014','JIMENEZ',391),('07','014','EL BOSQUE',392),('08','014','CORONADO',393),('09','014','BENITO JUAREZ',394),('10','014','MEZQUITAL',395),('11','014','DOLORES HIDALGO CUNA DE LA INDEPENDENCIA NACIONAL',396),('12','014','BENITO JUAREZ',397),('13','014','CALNALI',398),('14','014','ATOYAC',399),('15','014','ATLACOMULCO',400),('16','014','COAHUAYANA',401),('17','014','MAZATEPEC',402),('18','014','SANTA MARIA DEL ORO',403),('19','014','DOCTOR ARROYO',404),('20','014','CIUDAD IXTEPEC',405),('21','014','AMIXTLAN',406),('22','014','QUERETARO',407),('24','014','COXCATLAN',408),('25','014','ROSARIO',409),('26','014','BAVIACORA',410),('27','014','PARAISO',411),('28','014','GUERRERO',412),('29','014','HUEYOTLIPAN',413),('30','014','AMATLAN DE LOS REYES',414),('31','014','CUNCUNUL',415),('32','014','GENERAL FRANCISCO R. MURGUIA',416),('05','015','JUAREZ',417),('07','015','CACAHOATAN',418),('08','015','COYAME DEL SOTOL',419),('09','015','CUAUHTEMOC',420),('10','015','NAZAS',421),('11','015','GUANAJUATO',422),('12','015','BUENAVISTA DE CUELLAR',423),('13','015','CARDONAL',424),('14','015','AUTLAN DE NAVARRO',425),('15','015','ATLAUTLA',426),('16','015','COALCOMAN DE VAZQUEZ PALLARES',427),('17','015','MIACATLAN',428),('18','015','SANTIAGO IXCUINTLA',429),('19','015','DOCTOR COSS',430),('20','015','COATECAS ALTAS',431),('21','015','AMOZOC',432),('22','015','SAN JOAQUIN',433),('24','015','CHARCAS',434),('25','015','SALVADOR ALVARADO',435),('26','015','BAVISPE',436),('27','015','TACOTALPA',437),('28','015','GUSTAVO DIAZ ORDAZ',438),('29','015','IXTACUIXTLA DE MARIANO MATAMOROS',439),('30','015','ANGEL R. CABADA',440),('31','015','CUZAMA',441),('32','015','EL PLATEADO DE JOAQUIN AMARO',442),('05','016','LAMADRID',443),('07','016','CATAZAJA',444),('08','016','LA CRUZ',445),('09','016','MIGUEL HIDALGO',446),('10','016','NOMBRE DE DIOS',447),('11','016','HUANIMARO',448),('12','016','COAHUAYUTLA DE JOSE MARIA IZAZAGA',449),('13','016','CUAUTEPEC DE HINOJOSA',450),('14','016','AYOTLAN',451),('15','016','AXAPUSCO',452),('16','016','COENEO',453),('17','016','OCUITUCO',454),('18','016','TECUALA',455),('19','016','DOCTOR GONZALEZ',456),('20','016','COICOYAN DE LAS FLORES',457),('21','016','AQUIXTLA',458),('22','016','SAN JUAN DEL RIO',459),('24','016','EBANO',460),('25','016','SAN IGNACIO',461),('26','016','BENJAMIN HILL',462),('27','016','TEAPA',463),('28','016','HIDALGO',464),('29','016','IXTENCO',465),('30','016','LA ANTIGUA',466),('31','016','CHACSINKIN',467),('32','016','GENERAL PANFILO NATERA',468),('05','017','MATAMOROS',469),('07','017','CINTALAPA',470),('08','017','CUAUHTEMOC',471),('09','017','VENUSTIANO CARRANZA',472),('10','017','OCAMPO',473),('11','017','IRAPUATO',474),('12','017','COCULA',475),('13','017','CHAPANTONGO',476),('14','017','AYUTLA',477),('15','017','AYAPANGO',478),('16','017','CONTEPEC',479),('17','017','PUENTE DE IXTLA',480),('18','017','TEPIC',481),('19','017','GALEANA',482),('20','017','LA COMPA¥IA',483),('21','017','ATEMPAN',484),('22','017','TEQUISQUIAPAN',485),('24','017','GUADALCAZAR',486),('25','017','SINALOA',487),('26','017','CABORCA',488),('27','017','TENOSIQUE',489),('28','017','JAUMAVE',490),('29','017','MAZATECOCHCO DE JOSE MARIA MORELOS',491),('30','017','APAZAPAN',492),('31','017','CHANKOM',493),('32','017','GUADALUPE',494),('05','018','MONCLOVA',495),('07','018','COAPILLA',496),('08','018','CUSIHUIRIACHI',497),('10','018','EL ORO',498),('11','018','JARAL DEL PROGRESO',499),('12','018','COPALA',500),('13','018','CHAPULHUACAN',501),('14','018','LA BARCA',502),('15','018','CALIMAYA',503),('16','018','COPANDARO',504),('17','018','TEMIXCO',505),('18','018','TUXPAN',506),('19','018','GARCIA',507),('20','018','CONCEPCION BUENAVISTA',508),('21','018','ATEXCAL',509),('22','018','TOLIMAN',510),('24','018','HUEHUETLAN',511),('25','018','NAVOLATO',512),('26','018','CAJEME',513),('28','018','JIMENEZ',514),('29','018','CONTLA DE JUAN CUAMATZI',515),('30','018','AQUILA',516),('31','018','CHAPAB',517),('32','018','HUANUSCO',518),('05','019','MORELOS',519),('07','019','COMITAN DE DOMINGUEZ',520),('08','019','CHIHUAHUA',521),('10','019','OTAEZ',522),('11','019','JERECUARO',523),('12','019','COPALILLO',524),('13','019','CHILCUAUTLA',525),('14','019','BOLA¥OS',526),('15','019','CAPULHUAC',527),('16','019','COTIJA',528),('17','019','TEPALCINGO',529),('18','019','LA YESCA',530),('19','019','SAN PEDRO GARZA GARCIA',531),('20','019','CONCEPCION PAPALO',532),('21','019','ATLIXCO',533),('24','019','LAGUNILLAS',534),('26','019','CANANEA',535),('28','019','LLERA',536),('29','019','TEPETITLA DE LARDIZABAL',537),('30','019','ASTACINGA',538),('31','019','CHEMAX',539),('32','019','JALPA',540),('05','020','MUZQUIZ',541),('07','020','LA CONCORDIA',542),('08','020','CHINIPAS',543),('10','020','PANUCO DE CORONADO',544),('11','020','LEON',545),('12','020','COPANATOYAC',546),('13','020','ELOXOCHITLAN',547),('14','020','CABO CORRIENTES',548),('15','020','COACALCO DE BERRIOZABAL',549),('16','020','CUITZEO',550),('17','020','TEPOZTLAN',551),('18','020','BAHIA DE BANDERAS',552),('19','020','GENERAL BRAVO',553),('20','020','CONSTANCIA DEL ROSARIO',554),('21','020','ATOYATEMPAN',555),('24','020','MATEHUALA',556),('26','020','CARBO',557),('28','020','MAINERO',558),('29','020','SANCTORUM DE LAZARO CARDENAS',559),('30','020','ATLAHUILCO',560),('31','020','CHICXULUB PUEBLO',561),('32','020','JEREZ',562),('05','021','NADADORES',563),('07','021','COPAINALA',564),('08','021','DELICIAS',565),('10','021','PE¥ON BLANCO',566),('11','021','MOROLEON',567),('12','021','COYUCA DE BENITEZ',568),('13','021','EMILIANO ZAPATA',569),('14','021','CASIMIRO CASTILLO',570),('15','021','COATEPEC HARINAS',571),('16','021','CHARAPAN',572),('17','021','TETECALA',573),('19','021','GENERAL ESCOBEDO',574),('20','021','COSOLAPA',575),('21','021','ATZALA',576),('24','021','MEXQUITIC DE CARMONA',577),('26','021','LA COLORADA',578),('28','021','EL MANTE',579),('29','021','NANACAMILPA DE MARIANO ARISTA',580),('30','021','ATOYAC',581),('31','021','CHICHIMILA',582),('32','021','JIMENEZ DEL TEUL',583),('05','022','NAVA',584),('07','022','CHALCHIHUITAN',585),('08','022','DR. BELISARIO DOMINGUEZ',586),('10','022','POANAS',587),('11','022','OCAMPO',588),('12','022','COYUCA DE CATALAN',589),('13','022','EPAZOYUCAN',590),('14','022','CIHUATLAN',591),('15','022','COCOTITLAN',592),('16','022','CHARO',593),('17','022','TETELA DEL VOLCAN',594),('19','022','GENERAL TERAN',595),('20','022','COSOLTEPEC',596),('21','022','ATZITZIHUACAN',597),('24','022','MOCTEZUMA',598),('26','022','CUCURPE',599),('28','022','MATAMOROS',600),('29','022','ACUAMANALA DE MIGUEL HIDALGO',601),('30','022','ATZACAN',602),('31','022','CHIKINDZONOT',603),('32','022','JUAN ALDAMA',604),('05','023','OCAMPO',605),('07','023','CHAMULA',606),('08','023','GALEANA',607),('10','023','PUEBLO NUEVO',608),('11','023','PENJAMO',609),('12','023','CUAJINICUILAPA',610),('13','023','FRANCISCO I. MADERO',611),('14','023','ZAPOTLAN EL GRANDE',612),('15','023','COYOTEPEC',613),('16','023','CHAVINDA',614),('17','023','TLALNEPANTLA',615),('19','023','GENERAL TREVI¥O',616),('20','023','CUILAPAM DE GUERRERO',617),('21','023','ATZITZINTLA',618),('24','023','RAYON',619),('26','023','CUMPAS',620),('28','023','MENDEZ',621),('29','023','NATIVITAS',622),('30','023','ATZALAN',623),('31','023','CHOCHOLA',624),('32','023','JUCHIPILA',625),('05','024','PARRAS',626),('07','024','CHANAL',627),('08','024','SANTA ISABEL',628),('10','024','RODEO',629),('11','024','PUEBLO NUEVO',630),('12','024','CUALAC',631),('13','024','HUASCA DE OCAMPO',632),('14','024','COCULA',633),('15','024','CUAUTITLAN',634),('16','024','CHERAN',635),('17','024','TLALTIZAPAN DE ZAPATA',636),('19','024','GENERAL ZARAGOZA',637),('20','024','CUYAMECALCO VILLA DE ZARAGOZA',638),('21','024','AXUTLA',639),('24','024','RIOVERDE',640),('26','024','DIVISADEROS',641),('28','024','MIER',642),('29','024','PANOTLA',643),('30','024','TLALTETELA',644),('31','024','CHUMAYEL',645),('32','024','LORETO',646),('05','025','PIEDRAS NEGRAS',647),('07','025','CHAPULTENANGO',648),('08','025','GOMEZ FARIAS',649),('10','025','SAN BERNARDO',650),('11','025','PURISIMA DEL RINCON',651),('12','025','CUAUTEPEC',652),('13','025','HUAUTLA',653),('14','025','COLOTLAN',654),('15','025','CHALCO',655),('16','025','CHILCHOTA',656),('17','025','TLAQUILTENANGO',657),('19','025','GENERAL ZUAZUA',658),('20','025','CHAHUITES',659),('21','025','AYOTOXCO DE GUERRERO',660),('24','025','SALINAS',661),('26','025','EMPALME',662),('28','025','MIGUEL ALEMAN',663),('29','025','SAN PABLO DEL MONTE',664),('30','025','AYAHUALULCO',665),('31','025','DZAN',666),('32','025','LUIS MOYA',667),('05','026','PROGRESO',668),('07','026','CHENALHO',669),('08','026','GRAN MORELOS',670),('10','026','SAN DIMAS',671),('11','026','ROMITA',672),('12','026','CUETZALA DEL PROGRESO',673),('13','026','HUAZALINGO',674),('14','026','CONCEPCION DE BUENOS AIRES',675),('15','026','CHAPA DE MOTA',676),('16','026','CHINICUILA',677),('17','026','TLAYACAPAN',678),('19','026','GUADALUPE',679),('20','026','CHALCATONGO DE HIDALGO',680),('21','026','CALPAN',681),('24','026','SAN ANTONIO',682),('26','026','ETCHOJOA',683),('28','026','MIQUIHUANA',684),('29','026','SANTA CRUZ TLAXCALA',685),('30','026','BANDERILLA',686),('31','026','DZEMUL',687),('32','026','MAZAPIL',688),('05','027','RAMOS ARIZPE',689),('07','027','CHIAPA DE CORZO',690),('08','027','GUACHOCHI',691),('10','027','SAN JUAN DE GUADALUPE',692),('11','027','SALAMANCA',693),('12','027','CUTZAMALA DE PINZON',694),('13','027','HUEHUETLA',695),('14','027','CUAUTITLAN DE GARCIA BARRAGAN',696),('15','027','CHAPULTEPEC',697),('16','027','CHUCANDIRO',698),('17','027','TOTOLAPAN',699),('19','027','LOS HERRERAS',700),('20','027','CHIQUIHUITLAN DE BENITO JUAREZ',701),('21','027','CALTEPEC',702),('24','027','SAN CIRO DE ACOSTA',703),('26','027','FRONTERAS',704),('28','027','NUEVO LAREDO',705),('29','027','TENANCINGO',706),('30','027','BENITO JUAREZ',707),('31','027','DZIDZANTUN',708),('32','027','MELCHOR OCAMPO',709),('05','028','SABINAS',710),('07','028','CHIAPILLA',711),('08','028','GUADALUPE',712),('10','028','SAN JUAN DEL RIO',713),('11','028','SALVATIERRA',714),('12','028','CHILAPA DE ALVAREZ',715),('13','028','HUEJUTLA DE REYES',716),('14','028','CUAUTLA',717),('15','028','CHIAUTLA',718),('16','028','CHURINTZIO',719),('17','028','XOCHITEPEC',720),('19','028','HIGUERAS',721),('20','028','HEROICA CIUDAD DE EJUTLA DE CRESPO',722),('21','028','CAMOCUAUTLA',723),('24','028','SAN LUIS POTOSI',724),('26','028','GRANADOS',725),('28','028','NUEVO MORELOS',726),('29','028','TEOLOCHOLCO',727),('30','028','BOCA DEL RIO',728),('31','028','DZILAM DE BRAVO',729),('32','028','MEZQUITAL DEL ORO',730),('05','029','SACRAMENTO',731),('07','029','CHICOASEN',732),('08','029','GUADALUPE Y CALVO',733),('10','029','SAN LUIS DEL CORDERO',734),('11','029','SAN DIEGO DE LA UNION',735),('12','029','CHILPANCINGO DE LOS BRAVO',736),('13','029','HUICHAPAN',737),('14','029','CUQUIO',738),('15','029','CHICOLOAPAN',739),('16','029','CHURUMUCO',740),('17','029','YAUTEPEC',741),('19','029','HUALAHUISES',742),('20','029','ELOXOCHITLAN DE FLORES MAGON',743),('21','029','CAXHUACAN',744),('24','029','SAN MARTIN CHALCHICUAUTLA',745),('26','029','GUAYMAS',746),('28','029','OCAMPO',747),('29','029','TEPEYANCO',748),('30','029','CALCAHUALCO',749),('31','029','DZILAM GONZALEZ',750),('32','029','MIGUEL AUZA',751),('05','030','SALTILLO',752),('07','030','CHICOMUSELO',753),('08','030','GUAZAPARES',754),('10','030','SAN PEDRO DEL GALLO',755),('11','030','SAN FELIPE',756),('12','030','FLORENCIO VILLARREAL',757),('13','030','IXMIQUILPAN',758),('14','030','CHAPALA',759),('15','030','CHICONCUAC',760),('16','030','ECUANDUREO',761),('17','030','YECAPIXTLA',762),('19','030','ITURBIDE',763),('20','030','EL ESPINAL',764),('21','030','COATEPEC',765),('24','030','SAN NICOLAS TOLENTINO',766),('26','030','HERMOSILLO',767),('28','030','PADILLA',768),('29','030','TERRENATE',769),('30','030','CAMERINO Z. MENDOZA',770),('31','030','DZITAS',771),('32','030','MOMAX',772),('05','031','SAN BUENAVENTURA',773),('07','031','CHILON',774),('08','031','GUERRERO',775),('10','031','SANTA CLARA',776),('11','031','SAN FRANCISCO DEL RINCON',777),('12','031','GENERAL CANUTO A. NERI',778),('13','031','JACALA DE LEDEZMA',779),('14','031','CHIMALTITAN',780),('15','031','CHIMALHUACAN',781),('16','031','EPITACIO HUERTA',782),('17','031','ZACATEPEC',783),('19','031','JUAREZ',784),('20','031','TAMAZULAPAM DEL ESPIRITU SANTO',785),('21','031','COATZINGO',786),('24','031','SANTA CATARINA',787),('26','031','HUACHINERA',788),('28','031','PALMILLAS',789),('29','031','TETLA DE LA SOLIDARIDAD',790),('30','031','CARRILLO PUERTO',791),('31','031','DZONCAUICH',792),('32','031','MONTE ESCOBEDO',793),('05','032','SAN JUAN DE SABINAS',794),('07','032','ESCUINTLA',795),('08','032','HIDALGO DEL PARRAL',796),('10','032','SANTIAGO PAPASQUIARO',797),('11','032','SAN JOSE ITURBIDE',798),('12','032','GENERAL HELIODORO CASTILLO',799),('13','032','JALTOCAN',800),('14','032','CHIQUILISTLAN',801),('15','032','DONATO GUERRA',802),('16','032','ERONGARICUARO',803),('17','032','ZACUALPAN',804),('19','032','LAMPAZOS DE NARANJO',805),('20','032','FRESNILLO DE TRUJANO',806),('21','032','COHETZALA',807),('24','032','SANTA MARIA DEL RIO',808),('26','032','HUASABAS',809),('28','032','REYNOSA',810),('29','032','TETLATLAHUCA',811),('30','032','CATEMACO',812),('31','032','ESPITA',813),('32','032','MORELOS',814),('05','033','SAN PEDRO',815),('07','033','FRANCISCO LEON',816),('08','033','HUEJOTITAN',817),('10','033','SUCHIL',818),('11','033','SAN LUIS DE LA PAZ',819),('12','033','HUAMUXTITLAN',820),('13','033','JUAREZ HIDALGO',821),('14','033','DEGOLLADO',822),('15','033','ECATEPEC DE MORELOS',823),('16','033','GABRIEL ZAMORA',824),('17','033','TEMOAC',825),('19','033','LINARES',826),('20','033','GUADALUPE ETLA',827),('21','033','COHUECAN',828),('24','033','SANTO DOMINGO',829),('26','033','HUATABAMPO',830),('28','033','RIO BRAVO',831),('29','033','TLAXCALA',832),('30','033','CAZONES DE HERRERA',833),('31','033','HALACHO',834),('32','033','MOYAHUA DE ESTRADA',835),('05','034','SIERRA MOJADA',836),('07','034','FRONTERA COMALAPA',837),('08','034','IGNACIO ZARAGOZA',838),('10','034','TAMAZULA',839),('11','034','SANTA CATARINA',840),('12','034','HUITZUCO DE LOS FIGUEROA',841),('13','034','LOLOTLA',842),('14','034','EJUTLA',843),('15','034','ECATZINGO',844),('16','034','HIDALGO',845),('19','034','MARIN',846),('20','034','GUADALUPE DE RAMIREZ',847),('21','034','CORONANGO',848),('24','034','SAN VICENTE TANCUAYALAB',849),('26','034','HUEPAC',850),('28','034','SAN CARLOS',851),('29','034','TLAXCO',852),('30','034','CERRO AZUL',853),('31','034','HOCABA',854),('32','034','NOCHISTLAN DE MEJIA',855),('05','035','TORREON',856),('07','035','FRONTERA HIDALGO',857),('08','035','JANOS',858),('10','035','TEPEHUANES',859),('11','035','SANTA CRUZ DE JUVENTINO ROSAS',860),('12','035','IGUALA DE LA INDEPENDENCIA',861),('13','035','METEPEC',862),('14','035','ENCARNACION DE DIAZ',863),('15','035','HUEHUETOCA',864),('16','035','LA HUACANA',865),('19','035','MELCHOR OCAMPO',866),('20','035','GUELATAO DE JUAREZ',867),('21','035','COXCATLAN',868),('24','035','SOLEDAD DE GRACIANO SANCHEZ',869),('26','035','IMURIS',870),('28','035','SAN FERNANDO',871),('29','035','TOCATLAN',872),('30','035','CITLALTEPETL',873),('31','035','HOCTUN',874),('32','035','NORIA DE ANGELES',875),('05','036','VIESCA',876),('07','036','LA GRANDEZA',877),('08','036','JIMENEZ',878),('10','036','TLAHUALILO',879),('11','036','SANTIAGO MARAVATIO',880),('12','036','IGUALAPA',881),('13','036','SAN AGUSTIN METZQUITITLAN',882),('14','036','ETZATLAN',883),('15','036','HUEYPOXTLA',884),('16','036','HUANDACAREO',885),('19','036','MIER Y NORIEGA',886),('20','036','GUEVEA DE HUMBOLDT',887),('21','036','COYOMEAPAN',888),('24','036','TAMASOPO',889),('26','036','MAGDALENA',890),('28','036','SAN NICOLAS',891),('29','036','TOTOLAC',892),('30','036','COACOATZINTLA',893),('31','036','HOMUN',894),('32','036','OJOCALIENTE',895),('05','037','VILLA UNION',896),('07','037','HUEHUETAN',897),('08','037','JUAREZ',898),('10','037','TOPIA',899),('11','037','SILAO',900),('12','037','IXCATEOPAN DE CUAUHTEMOC',901),('13','037','METZTITLAN',902),('14','037','EL GRULLO',903),('15','037','HUIXQUILUCAN',904),('16','037','HUANIQUEO',905),('19','037','MINA',906),('20','037','MESONES HIDALGO',907),('21','037','COYOTEPEC',908),('24','037','TAMAZUNCHALE',909),('26','037','MAZATAN',910),('28','037','SOTO LA MARINA',911),('29','037','ZILTLALTEPEC DE TRINIDAD SANCHEZ SANTOS',912),('30','037','COAHUITLAN',913),('31','037','HUHI',914),('32','037','PANUCO',915),('05','038','ZARAGOZA',916),('07','038','HUIXTAN',917),('08','038','JULIMES',918),('10','038','VICENTE GUERRERO',919),('11','038','TARANDACUAO',920),('12','038','ZIHUATANEJO DE AZUETA',921),('13','038','MINERAL DEL CHICO',922),('14','038','GUACHINANGO',923),('15','038','ISIDRO FABELA',924),('16','038','HUETAMO',925),('19','038','MONTEMORELOS',926),('20','038','VILLA HIDALGO',927),('21','038','CUAPIAXTLA DE MADERO',928),('24','038','TAMPACAN',929),('26','038','MOCTEZUMA',930),('28','038','TAMPICO',931),('29','038','TZOMPANTEPEC',932),('30','038','COATEPEC',933),('31','038','HUNUCMA',934),('32','038','PINOS',935),('07','039','HUITIUPAN',936),('08','039','LOPEZ',937),('10','039','NUEVO IDEAL',938),('11','039','TARIMORO',939),('12','039','JUAN R. ESCUDERO',940),('13','039','MINERAL DEL MONTE',941),('14','039','GUADALAJARA',942),('15','039','IXTAPALUCA',943),('16','039','HUIRAMBA',944),('19','039','MONTERREY',945),('20','039','HEROICA CIUDAD DE HUAJUAPAN DE LEON',946),('21','039','CUAUTEMPAN',947),('24','039','TAMPAMOLON CORONA',948),('26','039','NACO',949),('28','039','TULA',950),('29','039','XALOZTOC',951),('30','039','COATZACOALCOS',952),('31','039','IXIL',953),('32','039','RIO GRANDE',954),('07','040','HUIXTLA',955),('08','040','MADERA',956),('11','040','TIERRA BLANCA',957),('12','040','LEONARDO BRAVO',958),('13','040','LA MISION',959),('14','040','HOSTOTIPAQUILLO',960),('15','040','IXTAPAN DE LA SAL',961),('16','040','INDAPARAPEO',962),('19','040','PARAS',963),('20','040','HUAUTEPEC',964),('21','040','CUAUTINCHAN',965),('24','040','TAMUIN',966),('26','040','NACORI CHICO',967),('28','040','VALLE HERMOSO',968),('29','040','XALTOCAN',969),('30','040','COATZINTLA',970),('31','040','IZAMAL',971),('32','040','SAIN ALTO',972),('07','041','LA INDEPENDENCIA',973),('08','041','MAGUARICHI',974),('11','041','URIANGATO',975),('12','041','MALINALTEPEC',976),('13','041','MIXQUIAHUALA DE JUAREZ',977),('14','041','HUEJUCAR',978),('15','041','IXTAPAN DEL ORO',979),('16','041','IRIMBO',980),('19','041','PESQUERIA',981),('20','041','HUAUTLA DE JIMENEZ',982),('21','041','CUAUTLANCINGO',983),('24','041','TANLAJAS',984),('26','041','NACOZARI DE GARCIA',985),('28','041','VICTORIA',986),('29','041','PAPALOTLA DE XICOHTENCATL',987),('30','041','COETZALA',988),('31','041','KANASIN',989),('32','041','EL SALVADOR',990),('07','042','IXHUATAN',991),('08','042','MANUEL BENAVIDES',992),('11','042','VALLE DE SANTIAGO',993),('12','042','MARTIR DE CUILAPAN',994),('13','042','MOLANGO DE ESCAMILLA',995),('14','042','HUEJUQUILLA EL ALTO',996),('15','042','IXTLAHUACA',997),('16','042','IXTLAN',998),('19','042','LOS RAMONES',999),('20','042','IXTLAN DE JUAREZ',1000),('21','042','CUAYUCA DE ANDRADE',1001);
/*!40000 ALTER TABLE `gen_municipios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gen_usuarios`
--

DROP TABLE IF EXISTS `gen_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `gen_usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `ape_pat` varchar(20) NOT NULL,
  `ape_,mat` varchar(20) DEFAULT NULL,
  `correo` text NOT NULL,
  `curp` varchar(18) NOT NULL,
  `telefono` int(13) NOT NULL,
  `rol` varchar(15) NOT NULL,
  `password` varchar(35) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_usuarios`
--

LOCK TABLES `gen_usuarios` WRITE;
/*!40000 ALTER TABLE `gen_usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `gen_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gru_grupos`
--

DROP TABLE IF EXISTS `gru_grupos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `gru_grupos` (
  `id_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(15) NOT NULL,
  `turno` varchar(10) NOT NULL,
  `cupo_max` int(3) NOT NULL,
  `aula` varchar(5) NOT NULL,
  PRIMARY KEY (`id_grupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gru_grupos`
--

LOCK TABLES `gru_grupos` WRITE;
/*!40000 ALTER TABLE `gru_grupos` DISABLE KEYS */;
/*!40000 ALTER TABLE `gru_grupos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mat_det_materia`
--

DROP TABLE IF EXISTS `mat_det_materia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mat_det_materia` (
  `id_det_materia` int(11) NOT NULL AUTO_INCREMENT,
  `dia_imparte` varchar(10) NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_final` time NOT NULL,
  `id_docente` int(11) NOT NULL,
  `id_materia` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  PRIMARY KEY (`id_det_materia`),
  KEY `materia-docente_idx` (`id_docente`),
  KEY `det_materia-materia_idx` (`id_materia`),
  KEY `det_materia-grupo_idx` (`id_grupo`),
  CONSTRAINT `det_materia-docente` FOREIGN KEY (`id_docente`) REFERENCES `mat_docente` (`id_docente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `det_materia-grupo` FOREIGN KEY (`id_grupo`) REFERENCES `gru_grupos` (`id_grupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `det_materia-materia` FOREIGN KEY (`id_materia`) REFERENCES `mat_materia` (`id_materia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mat_det_materia`
--

LOCK TABLES `mat_det_materia` WRITE;
/*!40000 ALTER TABLE `mat_det_materia` DISABLE KEYS */;
/*!40000 ALTER TABLE `mat_det_materia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mat_docente`
--

DROP TABLE IF EXISTS `mat_docente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mat_docente` (
  `id_docente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `ape_pat` varchar(20) NOT NULL,
  `ape_mat` varchar(20) DEFAULT NULL,
  `telefono` int(13) NOT NULL,
  `correo` text,
  PRIMARY KEY (`id_docente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mat_docente`
--

LOCK TABLES `mat_docente` WRITE;
/*!40000 ALTER TABLE `mat_docente` DISABLE KEYS */;
/*!40000 ALTER TABLE `mat_docente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mat_materia`
--

DROP TABLE IF EXISTS `mat_materia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mat_materia` (
  `id_materia` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(15) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`id_materia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mat_materia`
--

LOCK TABLES `mat_materia` WRITE;
/*!40000 ALTER TABLE `mat_materia` DISABLE KEYS */;
/*!40000 ALTER TABLE `mat_materia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-13 19:32:31
